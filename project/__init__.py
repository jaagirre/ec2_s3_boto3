from flask import Flask


app = Flask(__name__, instance_relative_config=False)
with app.app_context():
    from . import routes


