import os

from flask import current_app as app
from flask import render_template, request, redirect, send_file

from .s3_functions import upload_file, show_image
from werkzeug.utils import secure_filename


UPLOAD_FOLDER = "uploads"
BUCKET = "jaagirre-image-data"



@app.route("/")
def home():
    return render_template('index.html')

@app.route("/ok")
def error():
    return render_template('ok.html')

@app.route("/error")
def ok():
    return render_template('error.html')

@app.route("/upload", methods=['POST'])
def upload():
    if request.method == "POST":
        try:
            f = request.files['file']
            f.save(os.path.join(UPLOAD_FOLDER, secure_filename(f.filename)))
            upload_file(f"uploads/{f.filename}", BUCKET)
            return redirect("/ok")
        except FileNotFoundError as e:
            return redirect("/error")

@app.route("/pics")
def list():
    contents = show_image(BUCKET)
    return render_template('collection.html', contents=contents)
