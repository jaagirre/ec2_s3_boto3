# ec2_s3_boto3

Demo Flask project to deploy in EC2 a flask application using Boto3 to upload/download images from/to S3

To develop and deploy this application ypu will required an AWS console access. In this course you will use the 
AWS Academy Learner LAB [Learner lab](https://awsacademy.instructure.com/courses/19852).



## Developing steps

First clone locally this repo;-) Open *PyCharm Professional* and create the python virtual environment for local development.
With pycharm it should be created automatically. If not execute the next commands in a pycharm terminal.

![](./doc/images/pycharm_venv.png)


```bash
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

Now try the installation. You can use one of the following ways to start the Flask service from the terminal.

```bash
export FLASK_APP=./project/__init__.py
joseba@mgep-jaagirre:~/src/macc/iraunkor/ec2_s3_boto3$ flask run
 * Serving Flask app './project/__init__.py' (lazy loading)
 * Environment: production
   WARNING: This is a development server. Do not use it in a production deployment.
   Use a production WSGI server instead.
 * Debug mode: off
 * Running on http://127.0.0.1:5000 (Press CTRL+C to quit)

```

The second way is to create a main() function that runs the Flask app. To achieve this create a manage.py file.

```bash
python manage.py
 * Debug mode: off
 * Running on all addresses (0.0.0.0)
   WARNING: This is a development server. Do not use it in a production deployment.
 * Running on http://127.0.0.1:5000
 * Running on http://192.168.0.12:5000 (Press CTRL+C to quit)
127.0.0.1 - - [16/May/2022 19:10:59] "GET / HTTP/1.1" 200 -
127.0.0.1 - - [16/May/2022 19:11:03] "GET /pics HTTP/1.1" 200 -

```


 
### Create a Flask application that interact with a S3 bucket

1. Create a S3 Bucket

2. Create an IAM user with S3FullAccess (You will use the temp  ~/.aws/credentials created in the Learner Lab)

```bash
aws configure 
AWS Access Key ID [****************QCUE]: ASIAQSMA
AWS Secret Access Key [****************zSPo]: v2bY3pn6CGN
Default region name [us-east-1]: 
Default output format [json]: 

~/gedit ~/.aws/credentials 
#paste the aws session token, this is just as a demo , in production better to assign a Role to a server!!!!!!!!!!!!!!!!!!!
```
3. Create the Flask Server Application : **project/__init__.py**
 
   - Create routes.py with the next routes
     - URL_ENDPOINT: "/" index.html
     - URL_ENDPOINT: "/upload" POST file to S3
     - URL_ENPOINT: "/pics" GET S3 presigned URLs

   - Make sure that you specify the name of the bucket that you have created
     - For example: **BUCKET = "jaagirre-image-data"**

4. Create S3 helper functions : **s3_functions.py**
    - upload()
    - show()
5. Create **uploads/** folder 

6. Run locally and test the application 
You can use one of the following ways to start the Flask service

```bash
export FLASK_APP=./project/__init__.py
joseba@mgep-jaagirre:~/src/macc/iraunkor/ec2_s3_boto3$ flask run
 * Serving Flask app './project/__init__.py' (lazy loading)
 * Environment: production
   WARNING: This is a development server. Do not use it in a production deployment.
   Use a production WSGI server instead.
 * Debug mode: off
 * Running on http://127.0.0.1:5000 (Press CTRL+C to quit)

```

The second way is to create a main() function that runs the Flask app. To achieve this create a manage.py file.

```bash
python manage.py
 * Debug mode: off
 * Running on all addresses (0.0.0.0)
   WARNING: This is a development server. Do not use it in a production deployment.
 * Running on http://127.0.0.1:5000
 * Running on http://192.168.0.12:5000 (Press CTRL+C to quit)
127.0.0.1 - - [16/May/2022 19:10:59] "GET / HTTP/1.1" 200 -
127.0.0.1 - - [16/May/2022 19:11:03] "GET /pics HTTP/1.1" 200 -

```

7. Test the application using the browser

- Upload images: **/**
- And view images: **/pics**

8. Debug the app

Create a Flask execution/debug configuration.

![](./doc/images/pycharm_flask_run_config.png)
![](./doc/images/pycharm_project_flask_settings.png)

Now execute the Debug and try to set different breakpoints;-)


### Local Development Dockerized 

To facilitate portability and improve the deployment process, a good action is to containerise the Flask application. 
For this you have a **Dockerfile** and **docker-compose.yml** in the repository.Analyse both the dockerfile and the docker-compose.
We will then run the application using the following command. Check the output of the container and try to navigate
to the server.


```bash
docker-compose up -d --build
Successfully built 74988d9326c6
Successfully tagged ec2_s3_boto3_app:latest
Creating s3_app ... done

```

```bash
docker-compose logs app
s3_app |    WARNING: This is a development server. Do not use it in a production deployment.
s3_app |  * Running on http://127.0.0.1:5000
s3_app |  * Running on http://10.10.1.2:5000 (Press CTRL+C to quit)
```

In order for the container to have AWS login credentials, and therefore access the S3 bucket, the aws credentials 
folder has been mounted as a docker volume. 

```dockerfile
  volumes:
      - .project:/app
      - $HOME/.aws/credentials:/root/.aws/credentials:ro
```
For local development and not having to create/execute the container at every code change,
the project's carpet adel has also been loaded as a volume. 


### Flask App Production Cloud deploy 

1. Create an Ubuntu EC2 instances => Use [AWS Academy Learner Lab](https://awsacademy.instructure.com/courses/19852) as the working console.
Make sure to download the ssh key you have created (.pem file).

2. Connect to EC2 instances via 
```bash
SSH ssh -i "../resources/mutliverse.pem" ubuntu@ec2-54-227-21-120.compute-1.amazonaws.com 
ubuntu@ip-172-31-31-84:~$ 
```

3. Once in the EC2 machine we have three possibilities to run the application: 

   - Clone the repository and create a virtualenv
   - Clone the code and create the container 
   - Do a docker pull and run it ;-)

Option: Clone the repository and create a virtualenv

```bash
sudo apt update
sudo apt install  python3-venv
git clone https://gitlab.com/macc_ci_cd/iraunkor/aws_cloud/ec2_s3_boto3
cd ec2_s3_boto3/
mkdir uploads
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
python3 manage.py
```
And now browse to the EC2 PUBLIC IP and port 5000.But you cannot get the page? why?
You haven't opened the 5000 port. Configure the instance Security group and add 5000 port.
Don't forget to set the credentials , or you can add a role to the server;-)

![](doc/images/EC2_PUBLIC_IP.png)
![](doc/images/ec2_sssecurity_group.png)
![](doc/images/securoty_group_config.png)
![](doc/images/ec2_running_ok.png)
![](doc/images/ec2_role.png)
![](doc/images/ec2_with_role_working.png)


Option :    - Clone the code and create the container 

```bash
sudo apt-get remove docker.io  docker
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin
```
Y ahora ejecutar el contenedor con roles
```bash
sudo docker compose up -d --build
```

To launch the production server with gunicorn and copying the files

```bash
sudo docker build -f Dockerfile.prod . -t multiverse_flask
sudo docker run -p 5000:5000 --env PORT=5000 multiverse_flask 
```
Option) Do a docker pull and run it ;-)Install docker

# TODO
